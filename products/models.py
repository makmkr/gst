from django.db import models

# Create your models here.
class Product(models.Model):
    #ITEM_TYPE_CHOICES = (('quatity':'Quantity'),
    #                    ('length':'Length'))

    brand    = models.CharField(max_length=500)
    item     = models.CharField(max_length=130)
    cp       = models.DecimalField(decimal_places=2, max_digits=10)
    # type     = models.CharField(max_length=120, choices=ITEM_TYPE_CHOICES, blank=True,null=True)
    quantity = models.CharField(max_length=120)
    sp       = models.DecimalField(decimal_places=2, max_digits=10)
    hsn      = models.BigIntegerField()

    def __str__(self):
        return self.item
