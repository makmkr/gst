from django.views.generic import ListView, DetailView
from django.shortcuts import render, get_object_or_404
from django.views.generic.edit import CreateView, UpdateView
from .models import Product
from django.urls import reverse, reverse_lazy
class ProductListView(ListView):
    queryset = Product.objects.all()
    template_name = "products/list.html"

    # def get_context_data(self, *args, **kwargs):
    #     context = super(ProductListView, self).get_context_data(*args, **kwargs)
    #     return context

# def product_list_view(request):
#     queryset = Product.objects.all()
#     context = {
#         'object_list': queryset
#     }
#     return render(request, "products/list.html", context)

class ProductDetailView(DetailView):
    queryset = Product.objects.all()
    template_name = "products/detail.html"

    # def get_context_data(self, *args, **kwargs):
    #     context = super(ProductListView, self).get_context_data(*args, **kwargs)
    #     return context


# def product_detail_view(request, pk=None, *args, **kwargs):
#     #instance = Product.objects.get(pk=pk)
#     instance = get_object_or_404(Product, pk=pk)
#     context = {
#         'object': instance
#     }
#     return render(request, "products/detail.html", context)


class AddProduct(CreateView):
    model = Product
    template_name = 'products/product_form.html'
    fields = ['item','brand','cp','sp','hsn','quantity']
    success_url = reverse_lazy('products:list')


#     def form_valid(self, form):
#         obj = form.save(commit=False)
#         obj.save()
#         if(obj.type=='quatity'):
#             success_url = reverse_lazy('products:update-quantity-product', kwargs={'pk':obj.id})
#         else:
#             success_url = reverse_lazy('products:update-length-product', kwargs={'pk':obj.id})
#
# class UpdateQuantityProduct(UpdateView):
#     model = Product
#     template_name = 'products/product_updateform.html'
#     fields = ['quantity']
#
#     success_url = reverse_lazy('products:list')
#
# class UpdateLengthProduct(UpdateView):
#     model = Product
#     template_name = 'products/product_updateform.html'
#     fields = ['quantity']
#
#     success_url = reverse_lazy('products:list')
