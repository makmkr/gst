# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-06-21 08:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('receipt', '0008_itemreceipt_new'),
    ]

    operations = [
        migrations.AlterField(
            model_name='itemreceipt',
            name='order_id',
            field=models.CharField(max_length=500),
        ),
    ]
