# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-06-21 07:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('receipt', '0003_auto_20180621_0757'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userreceipt',
            name='total_items',
            field=models.IntegerField(default=0),
        ),
    ]
