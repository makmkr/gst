from django.db import models
from products.models import Product
from gst.utils import unique_order_id_generator
from django.db.models.signals import pre_save

class ItemReceipt(models.Model):
   order_id = models.CharField(max_length=500)
   item     = models.ForeignKey(Product, blank=True,null=True)
   item_no  = models.IntegerField(default=0)
   new      = models.BooleanField(default=True)

   def __str__(self):
       return str(self.order_id)

class UserReceipt(models.Model):
    name        = models.CharField(max_length=60)
    order_id    = models.CharField(max_length=500)
    total_items = models.IntegerField(default=0)
    sub_total   = models.DecimalField(decimal_places=2,max_digits=10,default=0.00)
    date_time   = models.DateTimeField(auto_now_add=True)
    orders      = models.ManyToManyField(ItemReceipt)
    billed      = models.BooleanField(default=False)

    def __str__(self):
        return str(self.order_id)



def pre_save_create_order_id(sender, instance, *args, **kwargs):
    if not instance.order_id:
       instance.order_id = unique_order_id_generator(instance)

pre_save.connect(pre_save_create_order_id, sender=UserReceipt)
