from django.conf import settings
from django.conf.urls.static import static

from django.conf.urls import url, include

from .views import  CreateItemReceipt, UpdateUserReceipt, CreateUserReceipt
from .views import PrintReceipt

urlpatterns = [
    url(r'^item-receipt/(?P<pk>\d+)/$', CreateItemReceipt.as_view(), name='item-receipt'),
    url(r'^user-receipt/$', CreateUserReceipt.as_view(), name='user-recipt'),
    url(r'^update-user-receipt/(?P<pk>\d+)/$', UpdateUserReceipt.as_view(), name='update-user-receipt'),
    url(r'^print-receipt/$', PrintReceipt.as_view(), name='print-receipt'),
]

if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

