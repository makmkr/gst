from django.contrib import admin
from .models import ItemReceipt, UserReceipt
# Register your models here.
admin.site.register(ItemReceipt)
admin.site.register(UserReceipt)
