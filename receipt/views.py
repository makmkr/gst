from django.shortcuts import render
from .models import UserReceipt, ItemReceipt
from django.shortcuts import redirect
from products.models import Product
from django.views.generic.edit import CreateView, UpdateView
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView


class CreateUserReceipt(CreateView):
    model = UserReceipt
    template_name = 'receipt/receipt_form.html'
    fields = ['name']
    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.save()
        ItemReceipt.objects.create(order_id=obj.order_id)
        obj1 = ItemReceipt.objects.get(order_id=obj.order_id, new=True)
        return redirect(reverse('receipt:item-receipt', kwargs={'pk': obj1.id}))
    # success_url = reverse_lazy('receipt:item-receipt')

class CreateItemReceipt(UpdateView):
    model = ItemReceipt
    template_name = 'receipt/itemreceipt_form.html'
    fields = ['item_no']

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.new = False
        obj.save()
        obj1 = UserReceipt.objects.get(order_id=obj.order_id)
        return redirect(reverse('receipt:update-user-receipt', kwargs={'pk': obj1.id}))
    #success_url = reverse_lazy('receipt:update-user-receipt')

class UpdateUserReceipt(UpdateView):
    model = UserReceipt
    template_name = 'receipt/receipt_form.html'
    fields = []
    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.save()
        ItemReceipt.objects.create(order_id=obj.order_id)
        obj1 = ItemReceipt.objects.get(order_id=obj.order_id, new=True)
        return redirect(reverse('receipt:item-receipt', kwargs={'pk': obj1.id}))

    # success_url = reverse_lazy('receipt:item-receipt')
class PrintReceipt(ListView):
        template_name = 'receipt/print_receipt.html'
        def get_queryset(self):
            if (UserReceipt.objects.filter(billed=False)):
                obj = UserReceipt.objects.get(billed=False)
                obj1 = UserReceipt.objects.get(billed=False)
                obj.billed = True
                obj.save()

            if(UserReceipt.objects.filter(order_id=obj1.order_id)):
                return UserReceipt.objects.get(order_id=obj1.order_id)
            else:
                return UserReceipt.objects.all()