# Invoicing WEB APP

This Project is made in Django Framework of Python, and this is my first project in the Open Source World.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

Things you need to install the web app.

Virtualenv
Python 3.6 or above.
Django 1.11



### Installing



1. Install the virtualenv
2. Install the other dependencies.
3. Activate the virtualenv
4. Run the local sevrer by ``` python manage.py runserver```

## Authors

* **Rupesh Kumar & Piyush Singh** - *Initial work* - 



## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* All help from UDEMY Courses
* As it is our very first project into the Open Source World, we want to learn more and more and make the society run these web app efficiently.


